# -*- coding: utf-8 -*-
{
    'name': 'POS Ipay',
    'category': 'Sales/Point of Sale',
    'author': 'Nelson Mongare - SDL',
    'summary': 'Integrate your POS with Ipays POS terminal',
    'version': '0.9.1',
    'license': "LGPL-3",
    'sequence': 7,
    'description': """Ipay POS extention""",

    'depends': ['point_of_sale'],
    'data': [
        'views/pos_payment_method_views.xml',
        'views/point_of_sale_assets.xml',
        'views/res_config_settings_views.xml',
        'views/ipay_pos_config.xml',
        'security/ir.model.access.csv',
    ],
    'demo': [
        'views/ipay_pos_demo.xml',
    ],
    'qweb': [
        'static/src/xml/ipay_pos_view_form.xml',
        'static/src/xml/ipay_order_receipt.xml'
    ],
    "images": ['static/description/ipay_cover.jpg'],
    'installable': True,
}
